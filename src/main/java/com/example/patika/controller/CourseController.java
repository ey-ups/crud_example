package com.example.patika.controller;

import com.example.patika.model.Course;
import com.example.patika.service.CourseService;
import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }


    @GetMapping
    public List<Course> getCourses() {
        return courseService.getCourses();
    }

    @GetMapping("{id}")
    @ResponseBody
    public Course getCourse(@PathVariable Long id) throws NotFoundException {
        return courseService.getCourse(id);
    }

    @PostMapping
    public Long createCourse(@RequestBody Course course) {
        return courseService.createCourse(course);

    }


    @PutMapping("{id}")
    public String updateCourse(@RequestBody Course course, @PathVariable Long id) throws NotFoundException {
        return courseService.updateCourse(course,id);

    }

    @DeleteMapping("{id}")
    public String deleteCourse(@PathVariable Long id ) throws NotFoundException {
        return courseService.deleteCourse(id);
    }
}




