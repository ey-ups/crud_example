package com.example.patika.controller;

import com.example.patika.model.Student;
import com.example.patika.service.StudentService;
import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

   private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> getStudents() {
        return studentService.getStudents();
    }

    @GetMapping("{id}")
    @ResponseBody
    public Student getStudent(@PathVariable Long id) throws NotFoundException {
        return studentService.getStudent(id);
    }

    @PostMapping
    public Long createStudent(@RequestBody Student student) {
         return studentService.createStudent(student);

    }


    @PutMapping("{id}")
    public String updateStudent(@RequestBody Student student,@PathVariable Long id) throws NotFoundException {
        return studentService.updateStudent(student,id);

    }

    @DeleteMapping("{id}")
    public String deleteStudent(@PathVariable Long id ) throws NotFoundException {
        return studentService.deleteStudent(id);
    }


}
