package com.example.patika.controller;


import com.example.patika.model.Product;
import com.example.patika.repository.ProductRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }



    @GetMapping("")
    public List<Product> getProducts(){
        return productRepository.findAll();
    }

    @PostMapping("")
    public void setProduct(@RequestBody Product product){
        productRepository.save(product);
    }


}
