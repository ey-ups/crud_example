package com.example.patika.service;


import com.example.patika.model.Student;
import com.example.patika.repository.StudentRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }


    public Student getStudent(Long id) throws NotFoundException {
        Optional<Student> studentOptional = studentRepository.findById(id);
        return studentOptional.orElseThrow(() -> new NotFoundException("Student not found"));
    }


    public Long createStudent(Student student) {
        return studentRepository.save(student).getId();
    }

    public String updateStudent(Student student, Long id) throws NotFoundException {
        Optional<Student> studentOptional = studentRepository.findById(id);
        Student s = studentOptional.orElseThrow(() -> new NotFoundException("student was not found"));
        s.setDepartment(student.getDepartment());
        s.setNo(student.getNo());
        s.setSurname(student.getSurname());
        s.setName(student.getName());
        studentRepository.saveAndFlush(s);
        return id + " was changed";
    }

    public String deleteStudent(Long id) throws NotFoundException {
        Optional<Student> studentOptional = studentRepository.findById(id);
        Student s = studentOptional.orElseThrow(() -> new NotFoundException("student was not found"));
        studentRepository.delete(s);
        return id + " was deleted";
    }

}
