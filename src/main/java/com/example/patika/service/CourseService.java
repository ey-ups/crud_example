package com.example.patika.service;

import com.example.patika.model.Course;
import com.example.patika.repository.CourseRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> getCourses() {
        return courseRepository.findAll();
    }


    public Course getCourse(Long id) throws NotFoundException {
        Optional<Course> courseOptional = courseRepository.findById(id);
        return courseOptional.orElseThrow(() -> new NotFoundException("Course not found"));
    }


    public Long createCourse(Course course) {
        return courseRepository.save(course).getId();
    }

    public String updateCourse(Course course, Long id) throws NotFoundException {
        Optional<Course> courseOptional = courseRepository.findById(id);
        Course c = courseOptional.orElseThrow(() -> new NotFoundException("course was not found"));
        c.setCategory(course.getCategory());
        c.setName(course.getName());
        courseRepository.saveAndFlush(c);
        return id + " was changed";
    }

    public String deleteCourse(Long id) throws NotFoundException {
        Optional<Course> courseOptional = courseRepository.findById(id);
        Course s = courseOptional.orElseThrow(() -> new NotFoundException("course was not found"));
        courseRepository.delete(s);
        return id + " was deleted";
    }

}
